Task 1:
DML Operation (INSERT with SELECT)
Write a SQL statement to insert into a new table 'high_salary_employees' all employees who have a salary greater than $10000.

Task 2:
SELECT Statement with ORDER BY clause
Write a SQL statement to select all employees' first and last names and their salaries, sorted by salary in descending order.

Task 3:
Single Function (Date)
Write a SQL statement to calculate the number of years between the hire date and current date for all employees.

Task 4:
Aggregate Function with GROUP BY
Write a SQL statement to find the total salary paid for each department.


