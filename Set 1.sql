Task 1:
DDL Operation (CREATE)
Write a SQL statement to create a new table named 'new_employees' that has the same structure as the 'employees' table.

Task 2:
DML Operation (INSERT)
Write a SQL statement to insert a new record into the 'employees' table. Use your own values for the data.

Task 3:
SELECT Statement
Write a SQL statement to select all employees' first and last names and their department ids from the 'employees' table.

Task 4:
Single Function (Character)
Write a SQL statement to display the first name of all employees in lower case.

Task 5:
Aggregate Function
Write a SQL statement to find the average salary in the 'employees' table.
