--Data type conversion

SELECT TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS') AS xxxx FROM dual;

SELECT TO_CHAR(12345.67, '99999.99') AS yyyy FROM dual;

SELECT TO_NUMBER('12345.67', '99999.99') AS zzzz FROM dual;

SELECT TO_DATE('06/05/2024', 'DD/MM/YYYY') AS vvvv FROM dual;

--Dual

SELECT expression FROM DUAL;

SELECT SYSDATE FROM DUAL; 
SELECT 'Hello, World!' FROM DUAL;
SELECT 1 + 1 FROM DUAL;

--Where Condition
Equality Operators:
=: Equal to.
!= or <>: Not equal to.

Comparison Operators:
>: Greater than.
<: Less than.
>=: Greater than or equal to.
<=: Less than or equal to.

LIKE Operator:
LIKE: Selects values that match a pattern. Wildcard characters like % and _ can be used.
IN Operator:
IN: Selects values within a specified list.
NOT Operator:
NOT: Selects the opposite of a condition.
NULL Operator:
IS NULL: Selects NULL values.
IS NOT NULL: Selects non-NULL values.

Logical Operators:
AND: Selects records where both conditions are true.
OR: Selects records where at least one of the conditions is true.
NOT: Selects the opposite of a condition.



