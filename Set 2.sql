Task 1:
DDL Operation (ALTER)
Write a SQL statement to add a column 'middle_name' to the 'employees' table.

Task 2:
DML Operation (UPDATE)
Write a SQL statement to increase the salary of all employees by 10%.

Task 3:
SELECT Statement with WHERE clause
Write a SQL statement to select all employees who have a salary greater than $5000.

Task 4:
Single Function (Number)
Write a SQL statement to round the salary of all employees to the nearest whole number.

